package function;

import java.util.ArrayList;
import java.util.List;

public class FunctionLoader {
    public static List<Function> getFunctions() {
        List<Function> functions = new ArrayList<>(5);

        functions.add(new Function(
                "images/1.png",
                x -> 4*x*x*x - 5*x*x + 6*x - 7
        ));

        functions.add(new Function(
                "images/2.png",
                x -> Math.sin(x) + 1
        ));

        functions.add(new Function(
                "images/3.png",
                x -> 1/x
        ));

        functions.add(new Function(
                "images/4.png",
                x -> Math.exp(-x) + x
        ));

        functions.add(new Function(
                "images/5.png",
                x -> 1/(x*x)
        ));

        return functions;
    }
}
