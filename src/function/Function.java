package function;

import java.util.function.DoubleUnaryOperator;

public class Function {
    private final String image;
    private final DoubleUnaryOperator f;

    public Function(String image, DoubleUnaryOperator f) {
        this.image = image;
        this.f = f;
    }

    public String getImage() {
        return image;
    }
    public double f(double x) {
        return f.applyAsDouble(x);
    }
}
