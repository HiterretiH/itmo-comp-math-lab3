import function.Function;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class Application {
    private final JFrame frame;

    public Application(List<Function> functions) {
        frame = new JFrame("Lab 3");

        frame.setLayout(new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS));
        frame.getContentPane().setBackground(Color.WHITE);

        JLabel equationsLabel = new JLabel("Выберите функцию:");
        equationsLabel.setFont(new Font("Arial", Font.PLAIN, 24));
        frame.add(equationsLabel);
        frame.add(Box.createVerticalStrut(5));

        for (Function f : functions) {
            ImageIcon icon = new ImageIcon(f.getImage());
            JButton button = new JButton(icon);
            button.setMargin(new Insets(0, 0, 0, 0));
            button.setBackground(null);

            button.addActionListener(
                    e -> {
                        new FunctionWindow(f);
                    }
            );

            frame.add(button);
            frame.add(Box.createVerticalStrut(5));
        }

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
    }
}
