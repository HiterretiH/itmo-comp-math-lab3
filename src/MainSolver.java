import solvers.IntegralSolver;

public class MainSolver {
    private final IntegralSolver solver;

    public MainSolver(IntegralSolver solver) {
        this.solver = solver;
    }

    public Result solve(double from, double to, double epsilon) {
        int count = 4;
        double result_prev;
        double result = solver.solve(from, to, count);

        do {
            result_prev = result;
            count *= 2;
            result = solver.solve(from, to, count);
        } while(solver.calculateError(result_prev, result) > epsilon && count < 100000000);

        return new Result(count, result);
    }
}
