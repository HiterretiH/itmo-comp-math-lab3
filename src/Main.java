import function.FunctionLoader;

public class Main {
    public static void main(String[] args) {
        new Application(FunctionLoader.getFunctions());
    }
}