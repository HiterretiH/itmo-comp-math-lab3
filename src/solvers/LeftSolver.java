package solvers;

import function.Function;

public class LeftSolver extends IntegralSolver {
    private double from;
    private double to;
    private double halfStep;

    public LeftSolver(Function function) {
        super(function);
    }

    @Override
    public double solve(double from, double to, int count) {
        double result = 0;
        double step = (to - from) / count;

        this.from = from;
        this.to = to;
        this.halfStep = step * 0.5;

        double x = from;

        for (int i = 0; i < count; i++) {
            result += f(x);
            x += step;
        }

        return result * step;
    }

    private double f(double x) {
        double y = function.f(x);
        if (Math.abs(y) > 10000000 && x > from && x < to) {
            double left = function.f(x + halfStep);
            double right = function.f(x - halfStep);

            if (left > 0 && right < 0 || left < 0 && right > 0) {
                return 0;
            }
        }

        return y;
    }

    @Override
    public double calculateError(double prev, double cur) {
        return Math.abs(prev - cur);
    }
}
