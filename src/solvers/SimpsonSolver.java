package solvers;

import function.Function;

public class SimpsonSolver extends IntegralSolver {
    private double from;
    private double to;
    private double halfStep;

    public SimpsonSolver(Function function) {
        super(function);
    }

    @Override
    public double solve(double from, double to, int count) {
        double result = 0;
        double step = (to - from) / count;

        this.from = from;
        this.to = to;
        this.halfStep = step * 0.5;

        double x = from;
        double[] y = new double[count + 1];

        for (int i = 0; i < count; i++) {
            y[i] = f(x);
            x += step;
        }
        y[count] = f(x);

        result += y[0] + y[count];

        x = from + step;
        for (int i = 1; i < count; i += 2) {
            result += 4 * f(x);
            x += step * 2;
        }

        x = from + 2 * step;
        for (int i = 2; i < count - 1; i += 2) {
            result += 2 * f(x);
            x += step * 2;
        }

        return step / 3 * result;
    }

    private double f(double x) {
        double y = function.f(x);
        if (Math.abs(y) > 10000000 && x > from && x < to) {
            double left = function.f(x + halfStep);
            double right = function.f(x - halfStep);

            if (left > 0 && right < 0 || left < 0 && right > 0) {
                return 0;
            }
        }

        return y;
    }

    @Override
    public double calculateError(double prev, double cur) {
        return Math.abs(prev - cur) / 15;
    }
}
