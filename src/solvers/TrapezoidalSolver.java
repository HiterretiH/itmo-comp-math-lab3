package solvers;

import function.Function;

public class TrapezoidalSolver extends IntegralSolver {
    private double from;
    private double to;
    private double halfStep;

    public TrapezoidalSolver(Function function) {
        super(function);
    }

    @Override
    public double solve(double from, double to, int count) {
        double result = (f(from) + f(to)) * 0.5;
        double step = (to - from) / count;

        this.from = from;
        this.to = to;
        this.halfStep = step * 0.5;

        double x = from + step;

        for (int i = 1; i < count - 1; i++) {
            result += f(x);
            x += step;
        }

        return result * step;
    }

    private double f(double x) {
        double y = function.f(x);
        if (Math.abs(y) > 10000000 && x > from && x < to) {
            double left = function.f(x + halfStep);
            double right = function.f(x - halfStep);

            if (left > 0 && right < 0 || left < 0 && right > 0) {
                return 0;
            }
        }

        return y;
    }

    @Override
    public double calculateError(double prev, double cur) {
        return Math.abs(prev - cur) / 3;
    }
}
