package solvers;

import function.Function;

public abstract class IntegralSolver {
    protected final Function function;

    public IntegralSolver(Function function) {
        this.function = function;
    }

    public abstract double solve(double from, double to, int count);

    public abstract double calculateError(double prev, double cur);
}
